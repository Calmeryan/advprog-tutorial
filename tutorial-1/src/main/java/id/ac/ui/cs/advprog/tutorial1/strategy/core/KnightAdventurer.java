package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    //ToDo: Complete me
    private String alias;

    public KnightAdventurer(){
        alias = "Knight";
        this.setAttackBehavior(new AttackWithSword());
        this.setDefenseBehavior(new DefendWithArmor());
    }

    @Override
    public String getAlias() {
        return alias;
    }
}
