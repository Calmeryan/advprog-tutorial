package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    //ToDo: Complete me
    private String alias;

    public MysticAdventurer(){
        alias = "Mystic";
        this.setAttackBehavior(new AttackWithMagic());
        this.setDefenseBehavior(new DefendWithShield());
    }

    @Override
    public String getAlias() {
        return alias;
    }
}
