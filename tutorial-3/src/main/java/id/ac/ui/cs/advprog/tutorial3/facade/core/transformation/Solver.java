package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;



public class Solver {

    private AbyssalTransformation abyssalTransformation = new AbyssalTransformation();
    private CelestialTransformation celestialTransformation = new CelestialTransformation();
    private CaesarTransformation caesarTransformation = new CaesarTransformation();
    private Codex alphaCodex = AlphaCodex.getInstance();
    private Codex runicCodex = RunicCodex.getInstance();

    //
    public String encode(String text){
        Spell spell = new Spell(text, alphaCodex);

        spell = caesarTransformation.encode(spell);
        spell = celestialTransformation.encode(spell);
        spell = abyssalTransformation.encode(spell);
        Spell result = CodexTranslator.translate(spell, runicCodex);

        return result.getText();
    }

    //
    public String decode(String code){
        Spell spell = new Spell(code, runicCodex);

        spell = CodexTranslator.translate(spell, alphaCodex);
        spell = abyssalTransformation.decode(spell);
        spell = celestialTransformation.decode(spell);
        spell = caesarTransformation.decode(spell);

        return spell.getText();
    }


}
