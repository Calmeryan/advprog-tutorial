package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.FesteringGreed;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)
    private int count = 0;

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private BowRepository bowRepository;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        if (count == 0) {
            for (Bow bow : bowRepository.findAll()) {
                BowAdapter bowAdapter = new BowAdapter(bow);
                weaponRepository.save(bowAdapter);
            }
            for (Spellbook spellbook : spellbookRepository.findAll()) {
                SpellbookAdapter spellbookAdapter = new SpellbookAdapter(spellbook);
                weaponRepository.save(spellbookAdapter);
            }
            count += 1;
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon mockWeapon = weaponRepository.findByAlias(weaponName);
        String log = mockWeapon.getHolderName();
        log = log + " attacked with " + mockWeapon.getName();
        if (attackType == 0) {
            log = log + " (normal attack): " + mockWeapon.normalAttack();
            logRepository.addLog(log);
        }
        else if (attackType == 1) {
            log = log + " (charged attack): " + mockWeapon.chargedAttack();
            logRepository.addLog(log);
        }
        weaponRepository.save(mockWeapon);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
