package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation {
    private int key;

    public CaesarTransformation(int key){
        this.key = key;
    }

    public CaesarTransformation(){
        this(5);
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int n = text.length();
        int selector = encode ? 1 : -1;
        char[] res = new char[n];

        for(int i = 0; i < n; i++) {
            char oldChara = text.charAt(i);
            int newIndex = codex.getIndex(oldChara) + (key*selector);
            newIndex = newIndex < 0? newIndex +  codex.getCharSize() : newIndex % codex.getCharSize();
            res[i] = codex.getChar(newIndex);
        }
        return new Spell(new String(res), codex);

    }

}
