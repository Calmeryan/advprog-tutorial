package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean coolingDown;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.coolingDown = false;
    }

    public boolean getCoolingDown() {
        return coolingDown;
    }

    @Override
    public String normalAttack() {
        coolingDown = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (!coolingDown) {
            coolingDown = true;
            return spellbook.largeSpell();
        }
        else {
            coolingDown = false;
            return "Magic power not enough for large spell";
        }

    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
