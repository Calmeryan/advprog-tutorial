package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean aiming;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.aiming = false;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(aiming);
    }

    @Override
    public String chargedAttack() {
        if (aiming) {
            aiming = false;
            return "Disable aiming mode";
        }
        else {
            aiming = true;
            return "Enable aiming mode";
        }
    }

    public boolean getAiming(){
        return aiming;
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return bow.getHolderName();
    }
}
