package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class SolverTest {
    private Class<?> solverClass;
    private Solver solver;

    @BeforeEach
    public void setup() throws Exception{
        solverClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Solver");
        solver = new Solver();
    }

    @Test
    public void testSolverHasEncode() throws Exception{
        Method encode = solverClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = encode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, encode.getParameterCount());
        assertEquals("java.lang.String", encode.getGenericReturnType().getTypeName());
    }

    @Test
    public void testSolverHasDecode() throws Exception{
        Method decode = solverClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = decode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, decode.getParameterCount());
        assertEquals("java.lang.String", decode.getGenericReturnType().getTypeName());
    }

    @Test
    public void testEncodeMethodReturnTheCorrectValue(){
        String result = solver.encode("umami");
        assertEquals("&BB_A", result);
    }

    @Test
    public void testDecodeMethodReturnTheCorrectValue(){
        String result = solver.decode("&BB_A");
        assertEquals("umami", result);
    }
}
